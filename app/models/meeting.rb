# Class for a group meeting
# @attr Name [String] Name of the meeting being made.
# @attr Length [Integer] How long the meeting is, in intervals of 15 min.
# @attr Leader [String] The leader of the group.
class Meeting < ApplicationRecord
  validates :title, presence: true, length: { maximum: 50 }
  validates :length, presence: true, numericality: { greater_than: 0 }
  validates :leader, length: { maximum: 25 }
end
