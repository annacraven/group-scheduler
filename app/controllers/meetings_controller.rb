class MeetingsController < ApplicationController
  before_action :set_meeting, only: %i(show)

  def index
    @meetings = Meeting.all.order('created_at DESC')
  end

  def new
    @meeting = Meeting.new
  end

  def create
    @meeting = Meeting.new(meeting_params)

    if @meeting.save
      redirect_to meeting_path(@meeting), 
        notice: 'Meeting was successfully created.'
    else
      render 'new'
    end
  end

  def show; end

  private
    def set_meeting
      @meeting = Meeting.find(params[:id])
    end

    def meeting_params
      params.require(:meeting).permit(:title, :length, :leader)
    end
end
