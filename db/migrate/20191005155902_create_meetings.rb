class CreateMeetings < ActiveRecord::Migration[5.2]
  def change
    create_table :meetings do |t|
      t.string :title
      t.integer :length
      t.string :leader

      t.timestamps
    end
  end
end
