require "rails_helper"

RSpec.describe Meeting, type: :model do
  describe 'Validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_length_of(:title).is_at_most(50) }
    it { is_expected.to validate_presence_of(:length) }
    it { is_expected.to validate_numericality_of(:length).is_greater_than(0) }
    it { is_expected.to validate_length_of(:leader).is_at_most(25) }
  end

  describe 'Associations' do
    skip
  end
end
