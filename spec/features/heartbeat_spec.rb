require 'rails_helper'

RSpec.feature 'Basic Heartbeat Test' do
  it 'successfully visits the root path' do
    visit root_path
    expect(page).to have_content('Meetings')
  end
end
