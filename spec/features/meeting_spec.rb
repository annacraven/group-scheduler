require 'rails_helper'

RSpec.feature 'Meeting' do
  before { visit meetings_path }

  it 'can be created' do
    click_on 'Create New Meeting'
    fill_in 'Title', with: 'Test'
    fill_in 'Length', with: 1
    click_on 'Create Meeting'
    expect(page).to have_content('Meeting was successfully created.')
  end

  it 'can be showed' do
    meeting = create(:meeting)
    visit current_path
    click_on meeting.title
    expect(page).to have_content("Title: #{meeting.title}")
  end
end
