FactoryBot.define do
  factory :meeting do
    sequence(:title) { |n| "event#{n}" }
    length { 4 }
    leader { 'Leader' }
  end
end
