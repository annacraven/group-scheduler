Rails.application.routes.draw do
  root 'meetings#index'

  resources :meetings, only: %i(index new create show)
end
